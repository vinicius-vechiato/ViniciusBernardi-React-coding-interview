import { Button, TextField, Typography } from '@mui/material';
import { useState } from 'react';

interface InlineInputProps {
  text: string;
}

export const InlineInput = ({ text }: InlineInputProps) => {

  const [textEdited, setTextEdited] = useState<string>(text);
  const [isEditing, setIsEditing] = useState<boolean>(false);

  const handleClickTypography = () => {
    setIsEditing(true)
  };

  const handleEditText = (data: string) => {
    setTextEdited(data)
  }

  const handleSave = () => {
    setIsEditing(false)
  }

  return (
    <>
      {isEditing ? (
        <div>
            <TextField variant='outlined' label={textEdited} onChange={(e) => handleEditText(e.target.value)}/>
            <Button onClick={handleSave}>Save</Button>
        </div>
      ) : (
        <Typography
          variant="subtitle1"
          lineHeight="1rem"
          onClick={handleClickTypography}
        >
          {textEdited}
        </Typography>
      )}
    </>
  );
};
